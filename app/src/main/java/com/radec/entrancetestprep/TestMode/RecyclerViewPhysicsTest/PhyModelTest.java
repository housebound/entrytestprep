package com.radec.entrancetestprep.TestMode.RecyclerViewPhysicsTest;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 4/6/17.
 */

public class PhyModelTest implements Serializable {
    public String question_phy, optionA_phy,optionB_phy,optionC_phy,optionD_phy,correctOption_phy,no_phy,type_phy;
    public ImageView questionImg_phy,optionAImg_phy,optionBImg_phy,optionCImg_phy,optionDImg_phy;

    public PhyModelTest()
    {

    }
    PhyModelTest(String question_phy, String optionA_phy, String optionB_phy, String optionC_phy, String optionD_phy,
                 String correctOption_phy, String no_phy,ImageView questionImg_phy,
                 ImageView optionAImg_phy,ImageView optionBImg_phy,ImageView optionCImg_phy,ImageView optionDImg_phy,
                 String type_phy)
    {
        this.question_phy=question_phy;
        this.optionA_phy=optionA_phy;
        this.optionB_phy=optionB_phy;
        this.optionC_phy=optionC_phy;
        this.optionD_phy=optionD_phy;
        this.no_phy=no_phy;
        this.correctOption_phy=correctOption_phy;
        this.type_phy=type_phy;
        this.questionImg_phy=questionImg_phy;
        this.optionAImg_phy=optionAImg_phy;
        this.optionBImg_phy=optionBImg_phy;
        this.optionCImg_phy=optionCImg_phy;
        this.optionDImg_phy=optionDImg_phy;
    }

    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
