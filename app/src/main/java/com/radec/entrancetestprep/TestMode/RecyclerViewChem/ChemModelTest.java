package com.radec.entrancetestprep.TestMode.RecyclerViewChem;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 4/8/17.
 */

public class ChemModelTest implements Serializable {
    public String question_chem, optionA_chem,optionB_chem,optionC_chem,optionD_chem,correctOption_chem,no_chem,type_chem;
    public ImageView questionImg_chem,optionAImg_chem,optionBImg_chem,optionCImg_chem,optionDImg_chem;

    public ChemModelTest()
    {

    }
    ChemModelTest(String question_chem, String optionA_chem, String optionB_chem, String optionC_chem,
                  String optionD_chem, String correctOption_chem, String no_chem,ImageView questionImg_chem,
                  ImageView optionAImg_chem,ImageView optionBImg_chem,ImageView optionCImg_chem,ImageView optionDImg_chem,
                  String type_chem)
    {
        this.question_chem=question_chem;
        this.optionA_chem=optionA_chem;
        this.optionB_chem=optionB_chem;
        this.optionC_chem=optionC_chem;
        this.optionD_chem=optionD_chem;
        this.no_chem=no_chem;
        this.correctOption_chem=correctOption_chem;
        this.type_chem=type_chem;
        this.questionImg_chem=questionImg_chem;
        this.optionAImg_chem=optionAImg_chem;
        this.optionBImg_chem=optionBImg_chem;
        this.optionCImg_chem=optionCImg_chem;
        this.optionDImg_chem=optionDImg_chem;
    }

    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
