package com.radec.entrancetestprep.TestMode;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.radec.entrancetestprep.PracticeMode.SubjectSelectionMenu;
import com.radec.entrancetestprep.R;
import com.radec.entrancetestprep.TestMode.RecyclerViewPhysicsTest.Adapter_PhyTest;
import com.radec.entrancetestprep.TestMode.RecyclerViewPhysicsTest.PhyModelTest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static com.radec.entrancetestprep.TestMode.RecyclerViewPhysicsTest.Adapter_PhyTest.scoreTest_phy;

public class PhyTest extends AppCompatActivity {
    public Integer scoreTotalTestPhy = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phy_test);
        final Button submitscorePhy = (Button) findViewById(R.id.submit_scoreTest_phy);
        final Context context = this;

        submitscorePhy.setVisibility(View.GONE);

        Intent intent = getIntent();
        final String number = intent.getStringExtra("value");
        final String minutes = intent.getStringExtra("minutes");
        final String topic = intent.getStringExtra("topic");

        final ImageView exit_phy = (ImageView) findViewById(R.id.exit_phy);
        final ImageView exit_phy_final = (ImageView) findViewById(R.id.exit_phy_final);
        exit_phy_final.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(PhyTest.this, SubjectSelectionMenu.class);
                startActivity(intent1);
            }
        });

        int myValue = Integer.valueOf(number);
        int myMinutes = (Integer.valueOf(minutes)) * 60000;

        final List<PhyModelTest> data = new ArrayList<>();


        try {
            JSONObject object = new JSONObject(readJSONFromAsset());
            JSONArray jsonArray = object.getJSONArray("Physics");
            ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;
            int counter = 0;
            for (int i = 0; i < jsonArray.length(); i++) {

                PhyModelTest model = new PhyModelTest();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (counter != myValue+5) {
                    if (jsonObject.get("Topic").equals(topic)) {
                        Log.d("Details --> ", jsonObject.getString("Question"));
                        String question_val = jsonObject.getString("Question");
                        String optionA_val = jsonObject.getString("A");
                        String optionB_val = jsonObject.getString("B");
                        String optionC_val = jsonObject.getString("C");
                        String dataType_val = jsonObject.getString("Type");
                        String optionD_val = jsonObject.getString("D");
                        String correct_val = jsonObject.getString("Correct");

                        m_li = new HashMap<String, String>();

                        m_li.put("Question", question_val);
                        model.question_phy = m_li.get("Question");
                        m_li.put("A", optionA_val);
                        model.optionA_phy = m_li.get("A");
                        m_li.put("B", optionB_val);
                        model.optionB_phy = m_li.get("B");
                        m_li.put("C", optionC_val);
                        model.optionC_phy = m_li.get("C");
                        m_li.put("D", optionD_val);
                        model.optionD_phy = m_li.get("D");
                        m_li.put("Correct", correct_val);
                        model.correctOption_phy = m_li.get("Correct");
                        m_li.put("Type", dataType_val);
                        model.type_phy = m_li.get("Type");

                        list.add(m_li);
                        data.add(model);
                        counter++;
                    }
                    else if(topic.equals("All"))
                    {
                        Log.d("Details --> ", jsonObject.getString("Question"));
                        String question_val = jsonObject.getString("Question");
                        String optionA_val = jsonObject.getString("A");
                        String optionB_val = jsonObject.getString("B");
                        String optionC_val = jsonObject.getString("C");
                        String optionD_val = jsonObject.getString("D");
                        String dataType_val = jsonObject.getString("Type");
                        String correct_val = jsonObject.getString("Correct");

                        m_li = new HashMap<String, String>();


                        m_li.put("Question", question_val);
                        model.question_phy = m_li.get("Question");
                        m_li.put("A", optionA_val);
                        model.optionA_phy = m_li.get("A");
                        m_li.put("B", optionB_val);
                        model.optionB_phy = m_li.get("B");
                        m_li.put("C", optionC_val);
                        model.optionC_phy = m_li.get("C");
                        m_li.put("D", optionD_val);
                        model.optionD_phy = m_li.get("D");
                        m_li.put("Correct", correct_val);
                        model.correctOption_phy = m_li.get("Correct");
                        m_li.put("Type", dataType_val);
                        model.type_phy = m_li.get("Type");
                        list.add(m_li);
                        data.add(model);
                        counter++;
                    }
                    else
                    {
                        //do  nothing
                    }

                }
            }
            if(topic.equals("All"))
            {
                for(int k=0;k<5;k++)
                {
                    final Random rand = new Random();

                    int diceRoll = (rand.nextInt(data.size()-1)) +1 ;
                    if(diceRoll<data.size())
                        data.remove(diceRoll);
                }
            }
            else
            {
                for(int k=0;k<5;k++)
                {
                    final Random rand = new Random();

                    int diceRoll = (rand.nextInt(data.size())) +1 ;
                    if(diceRoll<data.size())
                        data.remove(diceRoll);
                }
            }

        } catch (JSONException e) {

            Toast.makeText(PhyTest.this, e.toString(), Toast.LENGTH_LONG).show();
        }
        submitscorePhy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scoreTotalTestPhy = totalScore();
                // Toast.makeText(getApplicationContext(), String.valueOf(scoreTotal),Toast.LENGTH_LONG).show();

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.setContentView(R.layout.dialog_score);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                TextView totalQs = (TextView) dialog.findViewById(R.id.dialog_total);
                totalQs.setText(number);

                TextView correct = (TextView) dialog.findViewById(R.id.dialog_score);
                Log.v("Total Score", String.valueOf(scoreTotalTestPhy));

                correct.setText(String.valueOf(scoreTotalTestPhy));

                dialog.show();

                Button seeResult = (Button) dialog.findViewById(R.id.btn_seeResult);
                seeResult.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        submitscorePhy.setVisibility(View.GONE);
                        exit_phy.setVisibility(View.GONE);
                        exit_phy_final.setVisibility(View.VISIBLE);


                    }
                });

                Button exit = (Button) dialog.findViewById(R.id.button_dialogExit);
                exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent1 = new Intent(PhyTest.this, SubjectSelectionMenu.class);
                        startActivity(intent1);
                    }
                });


            }
        });


        final TextView timer = (TextView) findViewById(R.id.timer_test_phy);

        RecyclerView recyclerViewTest = (RecyclerView) findViewById(R.id.recyclerViewPhyTest);
        Adapter_PhyTest adapter_phy = new Adapter_PhyTest(PhyTest.this, data, 1);
        recyclerViewTest.setAdapter(adapter_phy);
        recyclerViewTest.setLayoutManager(new LinearLayoutManager(PhyTest.this));

        new CountDownTimer(myMinutes, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("" + String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                timer.setVisibility(View.INVISIBLE);
                submitscorePhy.setVisibility(View.VISIBLE);
                RecyclerView recyclerViewTestTime = (RecyclerView) findViewById(R.id.recyclerViewPhyTest);
                Adapter_PhyTest adapter_phy = new Adapter_PhyTest(PhyTest.this, data, 0);
                recyclerViewTestTime.setAdapter(adapter_phy);
                recyclerViewTestTime.setLayoutManager(new LinearLayoutManager(PhyTest.this));
                exit_phy.setVisibility(View.GONE);

            }
        }.start();

        exit_phy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.setVisibility(View.INVISIBLE);
                submitscorePhy.setVisibility(View.VISIBLE);
                RecyclerView recyclerViewTestTime = (RecyclerView) findViewById(R.id.recyclerViewPhyTest);
                Adapter_PhyTest adapter_phy = new Adapter_PhyTest(PhyTest.this, data, 0);
                recyclerViewTestTime.setAdapter(adapter_phy);
                recyclerViewTestTime.setLayoutManager(new LinearLayoutManager(PhyTest.this));
                exit_phy.setVisibility(View.GONE);
            }
        });


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
            }
        }, 10000);


    }

    public String readJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("physics.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
        return json;

    }

    public int totalScore() {

        Integer totalScore = 0;
        Iterator it = scoreTest_phy.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();


            totalScore = totalScore + ((Integer) pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException


        }
        return totalScore;
    }


    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(PhyTest.this, SubjectSelectionMenu.class);
        startActivity(intent);
    }

}

