package com.radec.entrancetestprep.TestMode.RecyclerViewEngTest;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 4/8/17.
 */

public class EngModelTest implements Serializable {
    public String question_eng, optionA_eng,optionB_eng,optionC_eng,optionD_eng,correctOption_eng,no_eng,type_eng;
    public ImageView questionImg_eng,optionAImg_eng,optionBImg_eng,optionCImg_eng,optionDImg_eng;

    public EngModelTest()
    {

    }
    EngModelTest(String question_eng, String optionA_eng, String optionB_eng,
                 String optionC_eng, String optionD_eng, String correctOption_eng, String no_eng,
                 ImageView questionImg_eng, ImageView optionAImg_eng,ImageView optionBImg_eng,
                 ImageView optionCImg_eng,ImageView optionDImg_eng,
                 String type_eng)
    {
        this.question_eng=question_eng;
        this.optionA_eng=optionA_eng;
        this.optionB_eng=optionB_eng;
        this.optionC_eng=optionC_eng;
        this.optionD_eng=optionD_eng;
        this.no_eng=no_eng;
        this.correctOption_eng=correctOption_eng;
        this.type_eng=type_eng;
        this.correctOption_eng=correctOption_eng;
        this.questionImg_eng=questionImg_eng;
        this.optionAImg_eng=optionAImg_eng;
        this.optionBImg_eng=optionBImg_eng;
        this.optionCImg_eng=optionCImg_eng;
        this.optionDImg_eng=optionDImg_eng;
    }


    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
