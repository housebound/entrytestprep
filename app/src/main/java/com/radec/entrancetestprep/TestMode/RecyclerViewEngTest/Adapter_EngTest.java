package com.radec.entrancetestprep.TestMode.RecyclerViewEngTest;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.radec.entrancetestprep.R;
import com.radec.entrancetestprep.TestMode.RecyclerViewChem.Adapter_ChemTest;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.radec.entrancetestprep.TestMode.MathTest.choice;
import static jxl.common.LengthUnit.getCount;

/**
 * Created by tayyabataimur on 4/8/17.
 */

public class Adapter_EngTest extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static HashMap<String,Integer> scoreTest_eng=new HashMap<String, Integer>();


    private Context context;
    private LayoutInflater inflater;
    List<EngModelTest> eng_dataTest= Collections.emptyList();
    EngModelTest engModelTest;
    String correct_eng;
    int check_eng;




    public Adapter_EngTest(Context context, List<EngModelTest> eng_data,int check_eng)
    {
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.eng_dataTest=eng_data;
        this.check_eng=check_eng;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.card,parent,false);
        Adapter_EngTest.MyHolder holder=new Adapter_EngTest.MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final Adapter_EngTest.MyHolder myHolder = (Adapter_EngTest.MyHolder) holder;
        engModelTest = eng_dataTest.get(position);
        myHolder.serialNo.setText(String.valueOf(position + 1) + ". ");


        if (eng_dataTest.get(position).type_eng.equals(String.valueOf(0))) {

            myHolder.questionImg.setVisibility(View.INVISIBLE);
            myHolder.optionAImg.setVisibility(View.INVISIBLE);
            myHolder.optionBImg.setVisibility(View.INVISIBLE);
            myHolder.optionCImg.setVisibility(View.INVISIBLE);
            myHolder.optionDImg.setVisibility(View.INVISIBLE);

            myHolder.questionTxt.setText(engModelTest.question_eng);
            myHolder.optionATxt.setText(engModelTest.optionA_eng);
            myHolder.optionBTxt.setText(engModelTest.optionB_eng);
            myHolder.optionCTxt.setText(engModelTest.optionC_eng);
            myHolder.optionDTxt.setText(engModelTest.optionD_eng);
            ColorDrawable optionATxtColor=(ColorDrawable) myHolder.optionATxt.getBackground();
            ColorDrawable optionBTxtColor=(ColorDrawable) myHolder.optionATxt.getBackground();
            ColorDrawable optionCTxtColor=(ColorDrawable) myHolder.optionATxt.getBackground();
            ColorDrawable optionDTxtColor=(ColorDrawable) myHolder.optionATxt.getBackground();


            if(optionATxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionBTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionCTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionDTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }

            if (check_eng == 1) {


                myHolder.optionATxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        choice.put(String.valueOf(position), 1);


                        myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionATxt.setBackgroundColor(Color.parseColor("#CE93D8"));

                        correct_eng = "A";


                        if (correct_eng.equals(eng_dataTest.get(position).correctOption_eng)) {


                            scoreTest_eng.put(String.valueOf(position), 1);


                        } else {


                            scoreTest_eng.put(String.valueOf(position), 0);
                        }


                    }
                });

                myHolder.optionBTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        choice.put(String.valueOf(position), 2);

                        myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#CE93D8"));

                        correct_eng = "B";
                        if (correct_eng.equals(eng_dataTest.get(position).correctOption_eng)) {

                            scoreTest_eng.put(String.valueOf(position), 1);

                        } else {


                            scoreTest_eng.put(String.valueOf(position), 0);
                        }

                    }
                });

                myHolder.optionCTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        choice.put(String.valueOf(position), 3);

                        myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#CE93D8"));

                        correct_eng = "C";
                        if (correct_eng.equals(eng_dataTest.get(position).correctOption_eng)) {

                            scoreTest_eng.put(String.valueOf(position), 1);


                        } else {

                            scoreTest_eng.put(String.valueOf(position), 0);
                        }

                    }
                });

                myHolder.optionDTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        choice.put(String.valueOf(position), 4);

                        myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                        myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#CE93D8"));

                        correct_eng = "D";
                        if (correct_eng.equals(eng_dataTest.get(position).correctOption_eng)) {

                            scoreTest_eng.put(String.valueOf(position), 1);

                        } else {
                            scoreTest_eng.put(String.valueOf(position), 0);

                        }

                    }
                });

            } else if (check_eng == 0) {

                myHolder.optionAImg.setEnabled(false);
                myHolder.optionBImg.setEnabled(false);
                myHolder.optionCImg.setEnabled(false);
                myHolder.optionDImg.setEnabled(false);
                myHolder.questionImg.setEnabled(false);
                myHolder.optionATxt.setEnabled(false);
                myHolder.optionBTxt.setEnabled(false);
                myHolder.optionCTxt.setEnabled(false);
                myHolder.optionDTxt.setEnabled(false);
                myHolder.questionTxt.setEnabled(false);

                Iterator it = choice.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    System.out.println(pair.getKey() + " = " + pair.getValue());
                    if (pair.getKey().equals(String.valueOf(position))) {

                        if (pair.getValue().toString().equals("1")) {
                            myHolder.optionATxt.setBackgroundColor(Color.parseColor("#f44336"));

                        } else if (pair.getValue().toString().equals("2")) {
                            myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#f44336"));

                        } else if (pair.getValue().toString().equals("3")) {
                            myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#f44336"));

                        } else if (pair.getValue().toString().equals("4")) {


                            myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#f44336"));


                        }


                        it.remove();
                    }
                }

                String correct_testMath = eng_dataTest.get(position).correctOption_eng;
                if (correct_testMath.equals("A")) {
                    myHolder.optionATxt.setBackgroundColor(Color.parseColor("#66BB6A"));

                } else if (correct_testMath.equals("B")) {
                    myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#66BB6A"));

                } else if (correct_testMath.equals("C")) {
                    myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#66BB6A"));

                } else if (correct_testMath.equals("D")) {
                    myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#66BB6A"));

                }


            }
        }

        else {

            myHolder.questionImg.setVisibility(View.VISIBLE);
            myHolder.optionAImg.setVisibility(View.VISIBLE);
            myHolder.optionBImg.setVisibility(View.VISIBLE);
            myHolder.optionCImg.setVisibility(View.VISIBLE);
            myHolder.optionDImg.setVisibility(View.VISIBLE);

            myHolder.questionTxt.setVisibility(View.INVISIBLE);
            myHolder.optionATxt.setVisibility(View.INVISIBLE);
            myHolder.optionBTxt.setVisibility(View.INVISIBLE);
            myHolder.optionCTxt.setVisibility(View.INVISIBLE);
            myHolder.optionDTxt.setVisibility(View.INVISIBLE);


            String filename1 = eng_dataTest.get(position).question_eng;
            String baseDir1 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir1 + File.separator + filename1)).into(myHolder.questionImg);

            String filename2 = eng_dataTest.get(position).optionA_eng;
            String baseDir2 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir2 + File.separator + filename2)).into(myHolder.optionAImg);

            String filename3 = eng_dataTest.get(position).optionB_eng;
            String baseDir3 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir3 + File.separator + filename3)).into(myHolder.optionBImg);
            //Drawable drawable= ContextCompat.getDrawable(context,Integer.parseInt(s));
            //myHolder.optionAImg.setImageDrawable(drawable);

            String filename4 = eng_dataTest.get(position).optionA_eng;
            String baseDir4 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir4 + File.separator + filename4)).into(myHolder.optionCImg);

            String filename5 = eng_dataTest.get(position).optionA_eng;
            String baseDir5 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir5 + File.separator + filename5)).into(myHolder.optionDImg);
            //Toast.makeText(context,s,Toast.LENGTH_SHORT).show();
            Drawable drawableBack=Drawable.createFromPath(String.valueOf(R.drawable.border));
            final Drawable drawableCorrect=Drawable.createFromPath(String.valueOf(R.drawable.bored_correct));
            final Drawable drawableInc=Drawable.createFromPath(String.valueOf(R.drawable.border_incorrect));

            myHolder.questionImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));

            if(check_eng==1)
            {

                myHolder.optionAImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        choice.put(String.valueOf(position), 1);

                        myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_select));

                        correct_eng="A";

                        if(eng_dataTest.get(position).correctOption_eng.equals(correct_eng))
                        {
                            scoreTest_eng.put(String.valueOf(position),1);
                        }
                        else
                        {
                            scoreTest_eng.put(String.valueOf(position),0);

                        }


                    }
                });

                myHolder.optionBImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        choice.put(String.valueOf(position), 1);

                        myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_select));

                        correct_eng="B";

                        if(eng_dataTest.get(position).correctOption_eng.equals(correct_eng))
                        {
                            scoreTest_eng.put(String.valueOf(position),1);
                        }
                        else
                        {
                            scoreTest_eng.put(String.valueOf(position),0);

                        }


                    }
                });

                myHolder.optionCImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        choice.put(String.valueOf(position), 1);

                        myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_select));

                        correct_eng="C";

                        if(eng_dataTest.get(position).correctOption_eng.equals(correct_eng))
                        {
                            scoreTest_eng.put(String.valueOf(position),1);
                        }
                        else
                        {
                            scoreTest_eng.put(String.valueOf(position),0);

                        }


                    }
                });

                myHolder.optionDImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        choice.put(String.valueOf(position), 1);

                        myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                        myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_select));

                        correct_eng="D";

                        if(eng_dataTest.get(position).correctOption_eng.equals(correct_eng))
                        {
                            scoreTest_eng.put(String.valueOf(position),1);
                        }
                        else
                        {
                            scoreTest_eng.put(String.valueOf(position),0);

                        }


                    }
                });
            }

            else if(check_eng==0)
            { Iterator it = choice.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    System.out.println(pair.getKey() + " = " + pair.getValue());
                    if (pair.getKey().equals(String.valueOf(position))) {

                        if (pair.getValue().toString().equals("1")) {
                            myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_select));

                        } else if (pair.getValue().toString().equals("2")) {
                            myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_select));

                        } else if (pair.getValue().toString().equals("3")) {
                            myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_select));

                        } else if (pair.getValue().toString().equals("4")) {


                            myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_select));


                        }


                        it.remove();
                    }
                }

                String correct_testMath = eng_dataTest.get(position).correctOption_eng;
                if (correct_testMath.equals("A")) {
                    myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                } else if (correct_testMath.equals("B")) {
                    myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));
                } else if (correct_testMath.equals("C")) {
                    myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));
                } else if (correct_testMath.equals("D")) {
                    myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));
                }


            }

        }

    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return eng_dataTest.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{


        TextView questionTxt,serialNo;
        Button optionATxt,optionBTxt,optionCTxt,optionDTxt;
        ImageView questionImg,optionAImg,optionBImg,optionCImg,optionDImg;


        public MyHolder(View itemView) {
            super(itemView);

            questionTxt=(TextView)itemView.findViewById(R.id.question_txtView);
            optionATxt=(Button) itemView.findViewById(R.id.optionA);
            optionBTxt=(Button)itemView.findViewById(R.id.optionB);
            optionCTxt=(Button) itemView.findViewById(R.id.optionC);
            optionDTxt=(Button) itemView.findViewById(R.id.optionD);
            serialNo=(TextView)itemView.findViewById(R.id.serialNo);
            questionImg=(ImageView)itemView.findViewById(R.id.question_imgView);
            optionAImg=(ImageView)itemView.findViewById(R.id.optionAImg);
            optionBImg=(ImageView)itemView.findViewById(R.id.optionBImg);
            optionCImg=(ImageView)itemView.findViewById(R.id.optionCImg);
            optionDImg=(ImageView)itemView.findViewById(R.id.optionDImg);




        }
    }


}
