package com.radec.entrancetestprep.TestMode.RecyclerViewMathTest;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 2/17/17.
 */

public class MathModelTest implements Serializable {
    public String questionM, optionAM,optionBM,optionCM,optionDM,correctOptionM,noM, typeM;
    public ImageView questionImgM,optionAImgM,optionBImgM,optionCImgM,optionDImgM;

    public MathModelTest()
    {

    }
    MathModelTest(String questionM, String typeM, String optionAM, String optionBM, String optionCM, String optionDM, String correctOptionM, String noM,ImageView questionImgM,ImageView optionAImgM,ImageView optionBImgM,ImageView optionCImgM,ImageView optionDImgM)
    {
        this.questionM=questionM;
        this.optionAM=optionAM;
        this.optionBM=optionBM;
        this.optionCM=optionCM;
        this.optionDM=optionDM;
        this.noM=noM;
        this.correctOptionM=correctOptionM;
        this.questionImgM=questionImgM;
        this.optionAImgM=optionAImgM;
        this.optionBImgM=optionBImgM;
        this.optionCImgM=optionCImgM;
        this.optionDImgM=optionDImgM;
        this.typeM=typeM;
    }

    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
