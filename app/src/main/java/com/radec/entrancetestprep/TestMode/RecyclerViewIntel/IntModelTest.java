package com.radec.entrancetestprep.TestMode.RecyclerViewIntel;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 4/8/17.
 */

public class IntModelTest implements Serializable {
    public String question_int, optionA_int,optionB_int,optionC_int,optionD_int,correctOption_int,no_int,type_int;
    public ImageView questionImg_int,optionAImg_int,optionBImg_int,optionCImg_int,optionDImg_int;

    public IntModelTest()
    {

    }
    IntModelTest(String question_int, String optionA_int, String optionB_int, String optionC_int, String optionD_int,
                 String correctOption_int, String no_int,ImageView questionImg_int,
                 ImageView optionAImg_int,ImageView optionBImg_int,ImageView optionCImg_int,ImageView optionDImg_int,
                 String type_int)
    {
        this.question_int=question_int;
        this.optionA_int=optionA_int;
        this.optionB_int=optionB_int;
        this.optionC_int=optionC_int;
        this.optionD_int=optionD_int;
        this.no_int=no_int;
        this.correctOption_int=correctOption_int;
        this.type_int=type_int;
        this.questionImg_int=questionImg_int;
        this.optionAImg_int=optionAImg_int;
        this.optionBImg_int=optionBImg_int;
        this.optionCImg_int=optionCImg_int;
        this.optionDImg_int=optionDImg_int;
    }

    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
