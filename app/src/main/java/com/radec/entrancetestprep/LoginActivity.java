package com.radec.entrancetestprep;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.radec.entrancetestprep.PracticeMode.SubjectSelectionMenu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LoginActivity extends AppCompatActivity {
    Button login;
    EditText code;
    String barcode = "";
    String secureKey="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        secureKey= PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("Radec", "NotLoggedIn");

        String sd_cid = getSDCARDiD();
        //String sd_cid = "035344534430324780019acc7600844b";
        if (sd_cid.isEmpty()) {
            Toast.makeText(this, "No SDcard found..", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Generating key for you..", Toast.LENGTH_LONG).show();

            if (!sd_cid.isEmpty()) {
                int counter = 0;
                for (int i = 0; i < sd_cid.length(); i++) {
                    counter = counter + (i + 1);
                    if (counter >= sd_cid.length()) {
                        // do nothing

                    } else {
                        if (counter == 3) {
                            barcode = barcode + sd_cid.charAt(counter) + "@";

                        } else {
                            barcode = barcode + sd_cid.charAt(counter) + "!";
                        }
                    }

                }

                Log.v("SID1", barcode);
            }
        }

        code = (EditText) findViewById(R.id.passKey);
        login = (Button) findViewById(R.id.loginBtn);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (barcode.isEmpty()
                        ) {
                    // TODO GO TO NEXT SCREEN
                    Toast.makeText(getApplicationContext(), "Code not generated", Toast.LENGTH_SHORT).show();


                } else if (barcode.equals(code.getText().toString()) && !barcode.isEmpty()) {

                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("Radec", code.getText().toString()).commit();
                    Intent intent = new Intent(LoginActivity.this, SubjectSelectionMenu.class);//which class to go??????
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Login Code", Toast.LENGTH_SHORT).show();
                    //TODO  prompt user that he's not having valid sd card in his phone roger
                    //TODO prevent user from moving forward
                    //TODO Close the application
                }
            }
        });


    }


    String getSDCARDiD() {

        String memBlk = "";
        String sd_cid = "";
        try {

            File file = new File("/sys/block/mmcblk1");
            if (file.exists() && file.isDirectory()) {

                memBlk = "mmcblk1";
                Process cmd = Runtime.getRuntime().exec("cat /sys/block/" + memBlk + "/device/serial");
                BufferedReader br = new BufferedReader(new InputStreamReader(cmd.getInputStream()));
                sd_cid = br.readLine();

                sd_cid = sd_cid.substring(1);
                sd_cid = sd_cid.substring(1);
                Log.v("SID", sd_cid);
                if (secureKey.equals(sd_cid)) {

                    Intent intent = new Intent(LoginActivity.this, SubjectSelectionMenu.class);//which class to go??????
                    startActivity(intent);
                }
                return sd_cid;
            } else {
                //System.out.println("not a directory");
                Toast.makeText(getApplicationContext(), "Sorry No Sd Card Found..", Toast.LENGTH_LONG)
                        .show();
                return sd_cid;
            }


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sd_cid;


    }

}