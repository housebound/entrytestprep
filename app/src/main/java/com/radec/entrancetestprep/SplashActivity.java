package com.radec.entrancetestprep;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.radec.entrancetestprep.PracticeMode.SubjectSelectionMenu;

/**
 * Created by tayyabataimur on 4/8/17.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, SubjectSelectionMenu.class);
        startActivity(intent);
        finish();
    }
}
