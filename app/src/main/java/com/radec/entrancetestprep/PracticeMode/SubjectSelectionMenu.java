package com.radec.entrancetestprep.PracticeMode;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.radec.entrancetestprep.KeyboardUtils;
import com.radec.entrancetestprep.R;
import com.radec.entrancetestprep.TestMode.ChemTest;
import com.radec.entrancetestprep.TestMode.EnglishTest;
import com.radec.entrancetestprep.TestMode.IntgTest;
import com.radec.entrancetestprep.TestMode.MathTest;
import com.radec.entrancetestprep.TestMode.PhyTest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

import static com.radec.entrancetestprep.R.id.spinner;

public class SubjectSelectionMenu extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    ArrayList<String> mathsTopics = new ArrayList<>();
    ArrayList<String> englishTopics = new ArrayList<>();


    ArrayList<String> physicsTopics = new ArrayList<>();
    ArrayList<String> chemistryTopics = new ArrayList<>();
    ArrayList<String> intelligenceTopics = new ArrayList<>();
    Spinner spinner2;

    String selectedTopic = "";
    int pos;
    int qus;
    String topic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_selection_menu);
        mathsTopics.add("All");
        englishTopics.add("All");
        physicsTopics.add("All");
        chemistryTopics.add("All");
        intelligenceTopics.add("All");
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-1027797501781184/4528853553");
        final AdView mAdView = (AdView) findViewById(R.id.adViewworker);
         AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);
        Toast.makeText(getApplicationContext(),"Number of questions are limited in free version, purchase app to enjoy more than 5000 questions.",Toast.LENGTH_LONG).show();
        KeyboardUtils.addKeyboardToggleListener(this, new KeyboardUtils.SoftKeyboardToggleListener()
        {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible)
            {
                if(isVisible){
                    mAdView.setVisibility(View.INVISIBLE);

                }
                else
                {
                    mAdView.setVisibility(View.VISIBLE);

                }

            }
        });

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

        spinner2 = (Spinner) findViewById(R.id.spinner2);
        spinner2.setOnItemSelectedListener(this);

        ImageView info_icon=(ImageView) findViewById(R.id.info_icon);
        info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(SubjectSelectionMenu.this);
                dialog.setContentView(R.layout.dialog_info);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                Button dismiss=(Button)dialog.findViewById(R.id.dismiss);
                dismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

        final EditText noOfQuestions = (EditText) findViewById(R.id.noOfQs);
        noOfQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdView.setVisibility(View.INVISIBLE);

            }
        });

        final Button submitPractice = (Button) findViewById(R.id.practice_mode_btn);

        final Button submitTest = (Button) findViewById(R.id.test_mode_btn);

        submitPractice.setBackground(getResources().getDrawable(R.drawable.border));
        // submitPractice.setBackgroundColor(Color.parseColor("#4A148C"));
        submitTest.setBackgroundColor(Color.parseColor("#4A148C"));


        submitPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitPractice.setBackgroundColor(Color.parseColor("#6A1B9A"));
                submitPractice.setTextColor(Color.parseColor("#FFFFFF"));
                final String value = noOfQuestions.getText().toString();


                if (noOfQuestions.getText().toString().matches("")) {
                    noOfQuestions.setText("");
                    Toast.makeText(SubjectSelectionMenu.this, "Please enter number of questions", Toast.LENGTH_LONG).show();
                    submitPractice.setBackground(getResources().getDrawable(R.drawable.border));
                    submitPractice.setTextColor(Color.parseColor("#7B1FA2"));
                    return;

                }

                else if(Integer.valueOf(value)<=0)
                {
                    Toast.makeText(getApplicationContext(),"Number of questions can not be zero or less!",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    qus = Integer.valueOf(value);


                    if (qus >= 100) {
                        noOfQuestions.setText("");
                        Toast.makeText(SubjectSelectionMenu.this, "Question Limit of 100 exceeded", Toast.LENGTH_LONG).show();

                    } else {

                        if (pos == 2) {
                            Intent intent = new Intent(SubjectSelectionMenu.this, MathTestScreenActivity.class);
                            intent.putExtra("value", noOfQuestions.getText().toString());
                            intent.putExtra("topic", selectedTopic);

                            startActivity(intent);
                        } else if (pos == 1) {
                            // Toast.makeText(SubjectSelectionMenu.this,"Meow",Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SubjectSelectionMenu.this, PhysicsTestScreenActivity.class);
                            intent.putExtra("value", noOfQuestions.getText().toString());
                            intent.putExtra("topic", selectedTopic);

                            startActivity(intent);

                        } else if (pos == 3) {
                            Intent intent = new Intent(SubjectSelectionMenu.this, ChemTestScreenActivity.class);
                            intent.putExtra("value", noOfQuestions.getText().toString());
                            intent.putExtra("topic", selectedTopic);

                            startActivity(intent);
                        } else if (pos == 4) {
                            Intent intent = new Intent(SubjectSelectionMenu.this, IntelligenceTestScreenActivity.class);
                            intent.putExtra("value", noOfQuestions.getText().toString());
                            intent.putExtra("topic", selectedTopic);

                            startActivity(intent);
                        } else if (pos == 0) {
                            Intent intent = new Intent(SubjectSelectionMenu.this, EngTestScreenActivity.class);
                            intent.putExtra("value", noOfQuestions.getText().toString());
                            intent.putExtra("topic", selectedTopic);

                            startActivity(intent);
                        }
                    }
                }
            }
        });
        submitTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String value = noOfQuestions.getText().toString();


                if (noOfQuestions.getText().toString().matches("")) {

                    noOfQuestions.setText("");
                    Toast.makeText(SubjectSelectionMenu.this, "Please enter number of questions", Toast.LENGTH_SHORT).show();
                    submitTest.setBackgroundColor(Color.parseColor("#4A148C"));
                    submitTest.setTextColor(Color.parseColor("#FFFFFF"));
                    return;

                }
                else if(Integer.valueOf(value)<=0)
                {
                    Toast.makeText(getApplicationContext(),"Number of questions can not be zero or less!",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {

                    submitTest.setBackgroundColor(Color.parseColor("#6A1B9A"));
                    if (pos == 2)

                    {
                        final Dialog dialog = new Dialog(SubjectSelectionMenu.this);
                        dialog.setContentView(R.layout.dialog_timer);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        final EditText minutes = (EditText) dialog.findViewById(R.id.minutes);
                        Button cancel = (Button) dialog.findViewById(R.id.btn_cancel);
                        Button okay = (Button) dialog.findViewById(R.id.button_okay);

                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                submitTest.setBackgroundColor(Color.parseColor("#4A148C"));
                                submitTest.setTextColor(Color.parseColor("#FFFFFF"));

                            }
                        });

                        okay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (minutes.getText().toString().matches("")) {
                                    Toast.makeText(SubjectSelectionMenu.this, "Please enter number of minutes", Toast.LENGTH_LONG).show();

                                } else {


                                    if (Integer.valueOf(minutes.getText().toString()) > 100) {
                                        Toast.makeText(SubjectSelectionMenu.this, "Time Limit of 100 minutes exceeded", Toast.LENGTH_LONG).show();

                                    } else {
                                        Intent intent = new Intent(SubjectSelectionMenu.this, MathTest.class);
                                        intent.putExtra("value", noOfQuestions.getText().toString());
                                        intent.putExtra("minutes", minutes.getText().toString());
                                        intent.putExtra("topic", selectedTopic);

                                        startActivity(intent);
                                    }
                                }
                            }
                        });
                        dialog.show();

                    } else if (pos == 1) {
                        final Dialog dialog = new Dialog(SubjectSelectionMenu.this);
                        dialog.setContentView(R.layout.dialog_timer);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        final EditText minutes = (EditText) dialog.findViewById(R.id.minutes);
                        Button cancel = (Button) dialog.findViewById(R.id.btn_cancel);
                        Button okay = (Button) dialog.findViewById(R.id.button_okay);

                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                submitTest.setBackgroundColor(Color.parseColor("#4A148C"));
                                submitTest.setTextColor(Color.parseColor("#FFFFFF"));
                                dialog.dismiss();
                            }
                        });

                        okay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (minutes.getText().toString().matches("")) {
                                    Toast.makeText(SubjectSelectionMenu.this, "Please enter number of minutes", Toast.LENGTH_LONG).show();

                                } else {


                                    if (Integer.valueOf(minutes.getText().toString()) > 100) {
                                        Toast.makeText(SubjectSelectionMenu.this, "Time Limit of 100 minutes exceeded", Toast.LENGTH_LONG).show();

                                    } else {
                                        Intent intent = new Intent(SubjectSelectionMenu.this, PhyTest.class);
                                        intent.putExtra("value", noOfQuestions.getText().toString());
                                        intent.putExtra("minutes", minutes.getText().toString());
                                        intent.putExtra("topic", selectedTopic);

                                        startActivity(intent);
                                    }
                                }
                            }
                        });
                        dialog.show();
                    } else if (pos == 4) {
                        final Dialog dialog = new Dialog(SubjectSelectionMenu.this);
                        dialog.setContentView(R.layout.dialog_timer);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        final EditText minutes = (EditText) dialog.findViewById(R.id.minutes);
                        Button cancel = (Button) dialog.findViewById(R.id.btn_cancel);
                        Button okay = (Button) dialog.findViewById(R.id.button_okay);

                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                submitTest.setBackgroundColor(Color.parseColor("#4A148C"));
                                submitTest.setTextColor(Color.parseColor("#FFFFFF"));
                                dialog.dismiss();
                            }
                        });

                        okay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (minutes.getText().toString().matches("")) {
                                    Toast.makeText(SubjectSelectionMenu.this, "Please enter number of minutes", Toast.LENGTH_LONG).show();

                                } else {


                                    if (Integer.valueOf(minutes.getText().toString()) > 100) {
                                        Toast.makeText(SubjectSelectionMenu.this, "Time Limit of 100 minutes exceeded", Toast.LENGTH_LONG).show();

                                    } else {
                                        Intent intent = new Intent(SubjectSelectionMenu.this, IntgTest.class);
                                        intent.putExtra("value", noOfQuestions.getText().toString());
                                        intent.putExtra("minutes", minutes.getText().toString());
                                        intent.putExtra("topic", selectedTopic);

                                        startActivity(intent);
                                    }
                                }
                            }
                        });
                        dialog.show();
                    } else if (pos == 0) {

                        final Dialog dialog = new Dialog(SubjectSelectionMenu.this);
                        dialog.setContentView(R.layout.dialog_timer);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        final EditText minutes = (EditText) dialog.findViewById(R.id.minutes);
                        Button cancel = (Button) dialog.findViewById(R.id.btn_cancel);
                        Button okay = (Button) dialog.findViewById(R.id.button_okay);

                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                submitTest.setBackgroundColor(Color.parseColor("#4A148C"));
                                submitTest.setTextColor(Color.parseColor("#FFFFFF"));
                                dialog.dismiss();
                            }
                        });

                        okay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (minutes.getText().toString().matches("")) {
                                    Toast.makeText(SubjectSelectionMenu.this, "Please enter number of minutes", Toast.LENGTH_LONG).show();

                                } else {


                                    if (Integer.valueOf(minutes.getText().toString()) > 100) {
                                        Toast.makeText(SubjectSelectionMenu.this, "Time Limit of 100 minutes exceeded", Toast.LENGTH_LONG).show();

                                    } else {
                                        Intent intent = new Intent(SubjectSelectionMenu.this, EnglishTest.class);
                                        intent.putExtra("value", noOfQuestions.getText().toString());
                                        intent.putExtra("minutes", minutes.getText().toString());
                                        intent.putExtra("topic", selectedTopic);

                                        startActivity(intent);
                                    }
                                }
                            }
                        });
                        dialog.show();
                    } else if (pos == 3) {
                        final Dialog dialog = new Dialog(SubjectSelectionMenu.this);
                        dialog.setContentView(R.layout.dialog_timer);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        final EditText minutes = (EditText) dialog.findViewById(R.id.minutes);
                        Button cancel = (Button) dialog.findViewById(R.id.btn_cancel);
                        Button okay = (Button) dialog.findViewById(R.id.button_okay);

                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                submitTest.setBackgroundColor(Color.parseColor("#4A148C"));
                                submitTest.setTextColor(Color.parseColor("#FFFFFF"));
                                dialog.dismiss();
                            }
                        });

                        okay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (minutes.getText().toString().matches("")) {
                                    Toast.makeText(SubjectSelectionMenu.this, "Please enter number of minutes", Toast.LENGTH_LONG).show();

                                } else {


                                    if (Integer.valueOf(minutes.getText().toString()) > 100) {
                                        Toast.makeText(SubjectSelectionMenu.this, "Time Limit of 100 minutes exceeded", Toast.LENGTH_LONG).show();

                                    } else {
                                        Intent intent = new Intent(SubjectSelectionMenu.this, ChemTest.class);
                                        intent.putExtra("value", noOfQuestions.getText().toString());
                                        intent.putExtra("minutes", minutes.getText().toString());
                                        intent.putExtra("topic", selectedTopic);

                                        startActivity(intent);
                                    }
                                }
                            }
                        });
                        dialog.show();
                    }
                }
            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        switch (adapterView.getId()) {

            case spinner:
                if (i == 0) {
                    pos = 0;
                    try {
                        ArrayListPopulation(0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                if (i == 2) {
                    pos = 2;
                    try {
                        ArrayListPopulation(2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (i == 1) {
                    pos = 1;
                    try {
                        ArrayListPopulation(1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                if (i == 3) {
                    pos = 3;
                    try {
                        ArrayListPopulation(3);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (i == 4) {
                    pos = 4;
                    try {
                        ArrayListPopulation(4);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.spinner2:
                if (pos == 0) {
                    selectedTopic = englishTopics.get(i);
                } else if (pos == 1) {
                    selectedTopic = physicsTopics.get(i);

                } else if (pos == 2) {
                    selectedTopic = mathsTopics.get(i);
                } else if (pos == 3) {
                    selectedTopic = chemistryTopics.get(i);

                } else if (pos == 4) {
                    selectedTopic = intelligenceTopics.get(i);

                }
                Log.v("SelectedTopic", selectedTopic);
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onBackPressed() {

        moveTaskToBack(true);
        // Intent intent = new Intent(SubjectSelectionMenu.this, SubjectSelectionMenu.class);
        // startActivity(intent);
    }


    void ArrayListPopulation(int pos) throws JSONException {
        if (pos == 0) {
            JSONObject object = new JSONObject(readJSONFromAsset("english"));
            JSONArray jsonArray = object.getJSONArray("English");
            for (int i = 0; i < jsonArray.length(); i++) {


                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String topic = jsonObject.getString("Topic");
                if (!englishTopics.contains(topic))
                    englishTopics.add(topic);
            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, englishTopics); //selected item will look like a spinner set from XML

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(spinnerArrayAdapter);
        } else if (pos == 1) {

            JSONObject object = new JSONObject(readJSONFromAsset("physics"));
            JSONArray jsonArray = object.getJSONArray("Physics");
            for (int i = 0; i < jsonArray.length(); i++) {


                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String topic = jsonObject.getString("Topic");
                if (!physicsTopics.contains(topic))
                    physicsTopics.add(topic);
            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, physicsTopics); //selected item will look like a spinner set from XML

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(spinnerArrayAdapter);
        } else if (pos == 2) {


            JSONObject object = new JSONObject(readJSONFromAsset("maths"));
            JSONArray jsonArray = object.getJSONArray("Maths");
            for (int i = 0; i < jsonArray.length(); i++) {


                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String topic = jsonObject.getString("Topic");
                if (!mathsTopics.contains(topic))
                    mathsTopics.add(topic);
            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mathsTopics); //selected item will look like a spinner set from XML

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(spinnerArrayAdapter);

        } else if (pos == 3) {


            JSONObject object = new JSONObject(readJSONFromAsset("chem"));
            JSONArray jsonArray = object.getJSONArray("Chemistry");
            for (int i = 0; i < jsonArray.length(); i++) {


                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String topic = jsonObject.getString("Topic");
                if (!chemistryTopics.contains(topic))
                    chemistryTopics.add(topic);
            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, chemistryTopics); //selected item will look like a spinner set from XML

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(spinnerArrayAdapter);

        } else if (pos == 4) {


            JSONObject object = new JSONObject(readJSONFromAsset("intelligence"));
            JSONArray jsonArray = object.getJSONArray("Intelligence");
            for (int i = 0; i < jsonArray.length(); i++) {


                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String topic = jsonObject.getString("Topic");
                if (!intelligenceTopics.contains(topic))
                    intelligenceTopics.add(topic);
            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, intelligenceTopics); //selected item will look like a spinner set from XML

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(spinnerArrayAdapter);

        }


    }

    public String readJSONFromAsset(String filename) {
        String json = null;
        try {
            InputStream is = this.getAssets().open(filename + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
        return json;

    }

}
