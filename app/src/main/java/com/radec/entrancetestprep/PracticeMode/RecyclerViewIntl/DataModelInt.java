package com.radec.entrancetestprep.PracticeMode.RecyclerViewIntl;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 2/20/17.
 */

public class DataModelInt implements Serializable {
    public String questionI, optionAI,optionBI,optionCI,optionDI,correctOptionI,noI,typeI;
    public ImageView questionImgI,optionAImgI,optionBImgI,optionCImgI,optionDImgI;

    public DataModelInt()
    {

    }
    DataModelInt(String questionI,String optionAI,String optionBI,String optionCI,String optionDI,String correctOptionI,String noI,ImageView questionImgI,ImageView optionAImgI,ImageView optionBImgI,ImageView optionCImgI,ImageView optionDImgI, String typeI)
    {
        this.questionI=questionI;
        this.optionAI=optionAI;
        this.optionBI=optionBI;
        this.optionCI=optionCI;
        this.optionDI=optionDI;
        this.noI=noI;
        this.correctOptionI=correctOptionI;
        this.typeI=typeI;
        this.questionImgI=questionImgI;
        this.optionAImgI=optionAImgI;
        this.optionBImgI=optionBImgI;
        this.optionCImgI=optionCImgI;
        this.optionDImgI=optionDImgI;
    }

    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
