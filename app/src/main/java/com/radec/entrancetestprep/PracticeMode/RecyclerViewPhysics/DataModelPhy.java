package com.radec.entrancetestprep.PracticeMode.RecyclerViewPhysics;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 2/20/17.
 */

public class DataModelPhy implements Serializable {
    public String questionPhy, optionAPhy,optionBPhy,optionCPhy,optionDPhy,correctOptionPhy,noPhy,typePhy;
    public ImageView questionImgPhy,optionAImgPhy,optionBImgPhy,optionCImgPhy,optionDImgPhy;


    public DataModelPhy()
    {

    }
    DataModelPhy(String questionPhy,String optionAPhy,String optionBPhy,String optionCPhy,String optionDPhy,String correctOptionPhy,String noPhy,ImageView questionImgPhy,ImageView optionAImgPhy,ImageView optionBImgPhy,ImageView optionCImgPhy,ImageView optionDImgPhy, String typePhy)
    {
        this.questionPhy=questionPhy;
        this.optionAPhy=optionAPhy;
        this.optionBPhy=optionBPhy;
        this.optionCPhy=optionCPhy;
        this.optionDPhy=optionDPhy;
        this.noPhy=noPhy;
        this.correctOptionPhy=correctOptionPhy;
        this.typePhy=typePhy;
        this.questionImgPhy=questionImgPhy;
        this.optionAImgPhy=optionAImgPhy;
        this.optionBImgPhy=optionBImgPhy;
        this.optionCImgPhy=optionCImgPhy;
        this.optionDImgPhy=optionDImgPhy;
    }

    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
