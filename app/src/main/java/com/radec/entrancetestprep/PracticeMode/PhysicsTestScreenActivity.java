package com.radec.entrancetestprep.PracticeMode;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.radec.entrancetestprep.PracticeMode.RecyclerViewPhysics.AdapterPhy;
import com.radec.entrancetestprep.PracticeMode.RecyclerViewPhysics.DataModelPhy;
import com.radec.entrancetestprep.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.radec.entrancetestprep.PracticeMode.RecyclerViewPhysics.AdapterPhy.scorePhy;

public class PhysicsTestScreenActivity extends AppCompatActivity {
    public Integer scoreTotalPhy =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_physics_test_screen);

        final Button submitscorePhy = (Button)findViewById(R.id.submit_score_physics);

        final Context context=this;

        final Button exitPhy=(Button)findViewById(R.id.exit_phy_prac);
        exitPhy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(PhysicsTestScreenActivity.this,SubjectSelectionMenu.class);
                startActivity(intent);
            }
        });

        Intent intent=getIntent();
        final String numberPhy=intent.getStringExtra("value");
        final String topic = intent.getStringExtra("topic");


        int myValuePhy=Integer.valueOf(numberPhy);


        submitscorePhy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scoreTotalPhy = totalScore();
                // Toast.makeText(getApplicationContext(), String.valueOf(scoreTotal),Toast.LENGTH_LONG).show();

                final Dialog dialog=new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.setContentView(R.layout.dialog_score);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                TextView totalQs=(TextView)dialog.findViewById(R.id.dialog_total);
                totalQs.setText(numberPhy);

                TextView correct=(TextView)dialog.findViewById(R.id.dialog_score);
                Log.v("Total Score",String.valueOf(scoreTotalPhy));
                correct.setText(String.valueOf(scoreTotalPhy));

                dialog.show();

                Button seeResult=(Button)dialog.findViewById(R.id.btn_seeResult);
                seeResult.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        submitscorePhy.setVisibility(View.GONE);
                        exitPhy.setVisibility(View.VISIBLE);
                        dialog.dismiss();

                    }
                });

                Button exit=(Button)dialog.findViewById(R.id.button_dialogExit);
                exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent1=new Intent(PhysicsTestScreenActivity.this,SubjectSelectionMenu.class);
                        startActivity(intent1);
                    }
                });


            }
        });


        List<DataModelPhy>data=new ArrayList<>();
        try {

            JSONObject object = new JSONObject(readJSONFromAsset());
            JSONArray jsonArray = object.getJSONArray("Physics");
            ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;
            int counter = 0;
            for (int i = 0; i < jsonArray.length(); i++) {

                DataModelPhy model = new DataModelPhy();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (counter != myValuePhy + 5) {
                    if (jsonObject.get("Topic").equals(topic)) {
                        Log.d("Details --> ", jsonObject.getString("Question"));
                        String question_val = jsonObject.getString("Question");
                        String optionA_val = jsonObject.getString("A");
                        String optionB_val = jsonObject.getString("B");
                        String optionC_val = jsonObject.getString("C");
                        String optionD_val = jsonObject.getString("D");
                        String dataType_val = jsonObject.getString("Type");
                        String correct_val = jsonObject.getString("Correct");

                        m_li = new HashMap<String, String>();

                        m_li.put("Question", question_val);
                        model.questionPhy = m_li.get("Question");
                        m_li.put("A", optionA_val);
                        model.optionAPhy = m_li.get("A");
                        m_li.put("B", optionB_val);
                        model.optionBPhy = m_li.get("B");
                        m_li.put("C", optionC_val);
                        model.optionCPhy = m_li.get("C");
                        m_li.put("D", optionD_val);
                        model.optionDPhy = m_li.get("D");
                        m_li.put("Type", dataType_val);
                        model.typePhy = m_li.get("Type");
                        m_li.put("Correct", correct_val);
                        model.correctOptionPhy = m_li.get("Correct");
                        list.add(m_li);
                        data.add(model);
                        counter++;
                    } else if (topic.equals("All")) {
                        Log.d("Details --> ", jsonObject.getString("Question"));
                        String question_val = jsonObject.getString("Question");
                        String optionA_val = jsonObject.getString("A");
                        String optionB_val = jsonObject.getString("B");
                        String optionC_val = jsonObject.getString("C");
                        String optionD_val = jsonObject.getString("D");
                        String dataType_val = jsonObject.getString("Type");
                        String correct_val = jsonObject.getString("Correct");

                        m_li = new HashMap<String, String>();

                        m_li.put("Question", question_val);
                        model.questionPhy = m_li.get("Question");
                        m_li.put("A", optionA_val);
                        model.optionAPhy = m_li.get("A");
                        m_li.put("B", optionB_val);
                        model.optionBPhy = m_li.get("B");
                        m_li.put("C", optionC_val);
                        model.optionCPhy = m_li.get("C");
                        m_li.put("D", optionD_val);
                        model.optionDPhy = m_li.get("D");
                        m_li.put("Type", dataType_val);
                        model.typePhy = m_li.get("Type");
                        m_li.put("Correct", correct_val);
                        model.correctOptionPhy = m_li.get("Correct");
                        list.add(m_li);
                        data.add(model);
                        counter++;
                    } else {
                        //do  nothing
                    }
                }


            }
            if(topic.equals("All"))
            {
                for(int k=0;k<5;k++)
                {
                    final Random rand = new Random();

                    int diceRoll = (rand.nextInt(data.size()-1)) +1 ;
                    if(diceRoll<data.size())
                        data.remove(diceRoll);
                }
            }
            else
            {
                for(int k=0;k<5;k++)
                {
                    final Random rand = new Random();
                    if(((rand.nextInt(data.size())) + 1)>0 )
                    {
                        int diceRoll = (rand.nextInt(data.size())) + 1;
                        if(diceRoll<data.size())
                            data.remove(diceRoll);
                    }

                }
            }


        }
        catch (JSONException e)
        {

            Toast.makeText(PhysicsTestScreenActivity.this, e.toString(), Toast.LENGTH_LONG).show();
        }



        RecyclerView recyclerViewPhy=(RecyclerView)findViewById(R.id.recyclerView_phy);
        AdapterPhy adapterPhy=new AdapterPhy(PhysicsTestScreenActivity.this,data);
        recyclerViewPhy.setAdapter(adapterPhy);
        recyclerViewPhy.setLayoutManager(new LinearLayoutManager(PhysicsTestScreenActivity.this));



    }

    public String readJSONFromAsset()
    {
        String json=null;
        try{
            InputStream is=this.getAssets().open("physics.json");
            int size=is.available();
            byte[] buffer=new byte[size];
            is.read(buffer);
            is.close();
            json=new String(buffer,"UTF-8");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;

        }
        return json;

    }
    public int totalScore (){

        Integer totalScore = 0;
        Iterator it = scorePhy.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();


            totalScore = totalScore+ ((Integer) pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException




        }
        return  totalScore;
    }


    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(PhysicsTestScreenActivity.this, SubjectSelectionMenu.class);
        startActivity(intent);
    }
    void removeData(){

    }


}

