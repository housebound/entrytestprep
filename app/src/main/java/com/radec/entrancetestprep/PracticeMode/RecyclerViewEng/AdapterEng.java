package com.radec.entrancetestprep.PracticeMode.RecyclerViewEng;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.radec.entrancetestprep.PracticeMode.RecyclerViewChem.AdapterChem;
import com.radec.entrancetestprep.PracticeMode.RecyclerViewChem.DataModelChem;
import com.radec.entrancetestprep.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tayyabataimur on 2/20/17.
 */

public class AdapterEng extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static HashMap<String,Integer> scoreEng=new HashMap<String, Integer>();

    private Context context;
    private LayoutInflater inflater;
    List<DataModelEng> eng_data= Collections.emptyList();
    DataModelEng engModel;
    String correct;




    public AdapterEng(Context context,List<DataModelEng> eng_data)
    {
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.eng_data=eng_data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.card,parent,false);
        AdapterEng.MyHolder holder=new AdapterEng.MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final AdapterEng.MyHolder myHolder=(AdapterEng.MyHolder)holder;

        engModel=eng_data.get(position);
        myHolder.serialNoE.setText(String.valueOf(position + 1) + ". ");

        if(eng_data.get(position).typeE.equals(String.valueOf(0))) {

            myHolder.questionImgE.setVisibility(View.INVISIBLE);
            myHolder.optionAImgE.setVisibility(View.INVISIBLE);
            myHolder.optionBImgE.setVisibility(View.INVISIBLE);
            myHolder.optionCImgE.setVisibility(View.INVISIBLE);
            myHolder.optionDImgE.setVisibility(View.INVISIBLE);
            ColorDrawable optionATxtColor=(ColorDrawable) myHolder.optionATxtE.getBackground();
            ColorDrawable optionBTxtColor=(ColorDrawable) myHolder.optionATxtE.getBackground();
            ColorDrawable optionCTxtColor=(ColorDrawable) myHolder.optionATxtE.getBackground();
            ColorDrawable optionDTxtColor=(ColorDrawable) myHolder.optionATxtE.getBackground();


            if(optionATxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionBTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionCTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionDTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }

            myHolder.questionTxtE.setText(engModel.questionE);
            myHolder.optionATxtE.setText(engModel.optionAE);
            myHolder.optionBTxtE.setText(engModel.optionBE);
            myHolder.optionCTxtE.setText(engModel.optionCE);
            myHolder.optionDTxtE.setText(engModel.optionDE);

            final int[] count = new int[1];

            myHolder.optionATxtE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    myHolder.optionBTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtE.setBackgroundColor(Color.parseColor("#4A148C"));

                    correct = "A";
                    if (correct.equals(eng_data.get(position).correctOptionE)) {
                        myHolder.optionATxtE.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreEng.put(String.valueOf(position), 1);


                    } else {
                        // Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionATxtE.setBackgroundColor(Color.parseColor("#f44336"));

                        scoreEng.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionBTxtE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionATxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "B";
                    if (correct.equals(eng_data.get(position).correctOptionE)) {
                        myHolder.optionBTxtE.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreEng.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionBTxtE.setBackgroundColor(Color.parseColor("#f44336"));

                        scoreEng.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionCTxtE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "C";
                    if (correct.equals(eng_data.get(position).correctOptionE)) {
                        myHolder.optionCTxtE.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreEng.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");


                    } else {
                        //  Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionCTxtE.setBackgroundColor(Color.parseColor("#f44336"));

                        scoreEng.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionDTxtE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxtE.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "D";
                    if (correct.equals(eng_data.get(position).correctOptionE)) {
                        myHolder.optionDTxtE.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreEng.put(String.valueOf(position), 1);

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        scoreEng.put(String.valueOf(position), 0);
                        myHolder.optionDTxtE.setBackgroundColor(Color.parseColor("#f44336"));

                    }

                }
            });
        }

        else
        {
            myHolder.questionImgE.setVisibility(View.VISIBLE);
            myHolder.optionAImgE.setVisibility(View.VISIBLE);
            myHolder.optionBImgE.setVisibility(View.VISIBLE);
            myHolder.optionCImgE.setVisibility(View.VISIBLE);
            myHolder.optionDImgE.setVisibility(View.VISIBLE);

            myHolder.questionTxtE.setVisibility(View.INVISIBLE);
            myHolder.optionATxtE.setVisibility(View.INVISIBLE);
            myHolder.optionBTxtE.setVisibility(View.INVISIBLE);
            myHolder.optionCTxtE.setVisibility(View.INVISIBLE);
            myHolder.optionDTxtE.setVisibility(View.INVISIBLE);


            String filename1 = eng_data.get(position).questionE;
            String baseDir1 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir1 + File.separator + filename1)).into(myHolder.questionImgE);

            String filename2 = eng_data.get(position).optionAE;
            String baseDir2 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir2 + File.separator + filename2)).into(myHolder.optionAImgE);

            String filename3 = eng_data.get(position).optionBE;
            String baseDir3 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir3 + File.separator + filename3)).into(myHolder.optionBImgE);
            //Drawable drawable= ContextCompat.getDrawable(context,Integer.parseInt(s));
            //myHolder.optionAImg.setImageDrawable(drawable);

            String filename4 = eng_data.get(position).optionAE;
            String baseDir4 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir4 + File.separator + filename4)).into(myHolder.optionCImgE);

            String filename5 = eng_data.get(position).optionAE;
            String baseDir5 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir5 + File.separator + filename5)).into(myHolder.optionDImgE);
            //Toast.makeText(context,s,Toast.LENGTH_SHORT).show();
            Drawable drawableBack=Drawable.createFromPath(String.valueOf(R.drawable.border));
            final Drawable drawableCorrect=Drawable.createFromPath(String.valueOf(R.drawable.bored_correct));
            final Drawable drawableInc=Drawable.createFromPath(String.valueOf(R.drawable.border_incorrect));

            myHolder.questionImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionAImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionBImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionCImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionDImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));

            myHolder.optionAImgE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="A";
                    myHolder.optionBImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(eng_data.get(position).correctOptionE.equals(correct))
                    {
                        scoreEng.put(String.valueOf(position), 1);

                        myHolder.optionAImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreEng.put(String.valueOf(position), 0);

                        myHolder.optionAImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionBImgE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="B";
                    myHolder.optionAImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(eng_data.get(position).correctOptionE.equals(correct))
                    {
                        scoreEng.put(String.valueOf(position), 1);

                        myHolder.optionBImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreEng.put(String.valueOf(position), 0);

                        myHolder.optionBImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionCImgE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="C";
                    myHolder.optionAImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(eng_data.get(position).correctOptionE.equals(correct))
                    {
                        scoreEng.put(String.valueOf(position), 1);

                        myHolder.optionCImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreEng.put(String.valueOf(position), 0);

                        myHolder.optionCImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionDImgE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="D";
                    myHolder.optionAImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(eng_data.get(position).correctOptionE.equals(correct))
                    {
                        scoreEng.put(String.valueOf(position), 1);

                        myHolder.optionDImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreEng.put(String.valueOf(position), 0);

                        myHolder.optionDImgE.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });


        }


    }
    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return eng_data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{


        TextView questionTxtE,serialNoE;
        Button optionATxtE,optionBTxtE,optionCTxtE,optionDTxtE;
        ImageView questionImgE,optionAImgE,optionBImgE,optionCImgE,optionDImgE;

        public MyHolder(View itemView) {
            super(itemView);

            questionTxtE=(TextView)itemView.findViewById(R.id.question_txtView);
            optionATxtE=(Button) itemView.findViewById(R.id.optionA);
            optionBTxtE=(Button)itemView.findViewById(R.id.optionB);
            optionCTxtE=(Button) itemView.findViewById(R.id.optionC);
            optionDTxtE=(Button) itemView.findViewById(R.id.optionD);
            serialNoE=(TextView)itemView.findViewById(R.id.serialNo);
            questionImgE=(ImageView)itemView.findViewById(R.id.question_imgView);
            optionAImgE=(ImageView)itemView.findViewById(R.id.optionAImg);
            optionBImgE=(ImageView)itemView.findViewById(R.id.optionBImg);
            optionCImgE=(ImageView)itemView.findViewById(R.id.optionCImg);
            optionDImgE=(ImageView)itemView.findViewById(R.id.optionDImg);







        }
    }
}
