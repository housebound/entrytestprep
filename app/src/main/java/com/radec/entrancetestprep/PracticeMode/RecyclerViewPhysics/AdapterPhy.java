package com.radec.entrancetestprep.PracticeMode.RecyclerViewPhysics;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.radec.entrancetestprep.PracticeMode.RecyclerViewIntl.AdapterInt;
import com.radec.entrancetestprep.PracticeMode.RecyclerViewIntl.DataModelInt;
import com.radec.entrancetestprep.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tayyabataimur on 2/20/17.
 */

public class AdapterPhy extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static HashMap<String,Integer> scorePhy=new HashMap<String, Integer>();

    private Context context;
    private LayoutInflater inflater;
    List<DataModelPhy> phy_data= Collections.emptyList();
    DataModelPhy phyModel;
    String correct;




    public AdapterPhy(Context context,List<DataModelPhy> phy_data)
    {
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.phy_data=phy_data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.card,parent,false);
        AdapterPhy.MyHolder holder=new AdapterPhy.MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final AdapterPhy.MyHolder myHolder=(AdapterPhy.MyHolder)holder;

        phyModel=phy_data.get(position);
        myHolder.serialNoPhy.setText(String.valueOf(position + 1) + ". ");

        if(phy_data.get(position).typePhy.equals(String.valueOf(0))) {

            myHolder.questionPhymgI.setVisibility(View.INVISIBLE);
            myHolder.optionAPhymgI.setVisibility(View.INVISIBLE);
            myHolder.optionBPhymgI.setVisibility(View.INVISIBLE);
            myHolder.optionCImgI.setVisibility(View.INVISIBLE);
            myHolder.optionDImgI.setVisibility(View.INVISIBLE);
            ColorDrawable optionATxtColor=(ColorDrawable) myHolder.optionATxtPhy.getBackground();
            ColorDrawable optionBTxtColor=(ColorDrawable) myHolder.optionATxtPhy.getBackground();
            ColorDrawable optionCTxtColor=(ColorDrawable) myHolder.optionATxtPhy.getBackground();
            ColorDrawable optionDTxtColor=(ColorDrawable) myHolder.optionATxtPhy.getBackground();


            if(optionATxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionBTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionCTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionDTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }

            myHolder.questionTxtPhy.setText(phyModel.questionPhy);
            myHolder.optionATxtPhy.setText(phyModel.optionAPhy);
            myHolder.optionBTxtPhy.setText(phyModel.optionBPhy);
            myHolder.optionCTxtPhy.setText(phyModel.optionCPhy);
            myHolder.optionDTxtPhy.setText(phyModel.optionDPhy);

            final int[] count = new int[1];

            myHolder.optionATxtPhy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    myHolder.optionBTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));

                    correct = "A";
                    if (correct.equals(phy_data.get(position).correctOptionPhy)) {
                        myHolder.optionATxtPhy.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scorePhy.put(String.valueOf(position), 1);


                    } else {
                        // Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionATxtPhy.setBackgroundColor(Color.parseColor("#f44336"));

                        scorePhy.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionBTxtPhy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionATxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "B";
                    if (correct.equals(phy_data.get(position).correctOptionPhy)) {
                        myHolder.optionBTxtPhy.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scorePhy.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionBTxtPhy.setBackgroundColor(Color.parseColor("#f44336"));

                        scorePhy.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionCTxtPhy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "C";
                    if (correct.equals(phy_data.get(position).correctOptionPhy)) {
                        myHolder.optionCTxtPhy.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scorePhy.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");


                    } else {
                        //  Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionCTxtPhy.setBackgroundColor(Color.parseColor("#f44336"));

                        scorePhy.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionDTxtPhy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxtPhy.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "D";
                    if (correct.equals(phy_data.get(position).correctOptionPhy)) {
                        myHolder.optionDTxtPhy.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scorePhy.put(String.valueOf(position), 1);

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        scorePhy.put(String.valueOf(position), 0);
                        myHolder.optionDTxtPhy.setBackgroundColor(Color.parseColor("#f44336"));

                    }

                }
            });
        }

        else
        {
            myHolder.questionPhymgI.setVisibility(View.VISIBLE);
            myHolder.optionAPhymgI.setVisibility(View.VISIBLE);
            myHolder.optionBPhymgI.setVisibility(View.VISIBLE);
            myHolder.optionCImgI.setVisibility(View.VISIBLE);
            myHolder.optionDImgI.setVisibility(View.VISIBLE);

            myHolder.questionTxtPhy.setVisibility(View.INVISIBLE);
            myHolder.optionATxtPhy.setVisibility(View.INVISIBLE);
            myHolder.optionBTxtPhy.setVisibility(View.INVISIBLE);
            myHolder.optionCTxtPhy.setVisibility(View.INVISIBLE);
            myHolder.optionDTxtPhy.setVisibility(View.INVISIBLE);


            String filename1 = phy_data.get(position).questionPhy;
            String baseDir1 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir1 + File.separator + filename1)).into(myHolder.questionPhymgI);

            String filename2 = phy_data.get(position).optionAPhy;
            String baseDir2 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir2 + File.separator + filename2)).into(myHolder.optionAPhymgI);

            String filename3 = phy_data.get(position).optionBPhy;
            String baseDir3 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir3 + File.separator + filename3)).into(myHolder.optionBPhymgI);
            //Drawable drawable= ContextCompat.getDrawable(context,Integer.parseInt(s));
            //myHolder.optionAPhymg.setImageDrawable(drawable);

            String filename4 = phy_data.get(position).optionAPhy;
            String baseDir4 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir4 + File.separator + filename4)).into(myHolder.optionCImgI);

            String filename5 = phy_data.get(position).optionAPhy;
            String baseDir5 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir5 + File.separator + filename5)).into(myHolder.optionDImgI);
            //Toast.makeText(context,s,Toast.LENGTH_SHORT).show();
            Drawable drawableBack=Drawable.createFromPath(String.valueOf(R.drawable.border));
            final Drawable drawableCorrect=Drawable.createFromPath(String.valueOf(R.drawable.bored_correct));
            final Drawable drawableInc=Drawable.createFromPath(String.valueOf(R.drawable.border_incorrect));

            myHolder.questionPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionAPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionBPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));

            myHolder.optionAPhymgI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="A";
                    myHolder.optionBPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(phy_data.get(position).correctOptionPhy.equals(correct))
                    {
                        scorePhy.put(String.valueOf(position), 1);

                        myHolder.optionAPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scorePhy.put(String.valueOf(position), 0);

                        myHolder.optionAPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionBPhymgI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="B";
                    myHolder.optionAPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(phy_data.get(position).correctOptionPhy.equals(correct))
                    {
                        scorePhy.put(String.valueOf(position), 1);

                        myHolder.optionBPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scorePhy.put(String.valueOf(position), 0);

                        myHolder.optionBPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionCImgI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="C";
                    myHolder.optionAPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(phy_data.get(position).correctOptionPhy.equals(correct))
                    {
                        scorePhy.put(String.valueOf(position), 1);

                        myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scorePhy.put(String.valueOf(position), 0);

                        myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionDImgI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="D";
                    myHolder.optionAPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBPhymgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(phy_data.get(position).correctOptionPhy.equals(correct))
                    {
                        scorePhy.put(String.valueOf(position), 1);

                        myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scorePhy.put(String.valueOf(position), 0);

                        myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });


        }


    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return phy_data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{


        TextView questionTxtPhy,serialNoPhy;
        Button optionATxtPhy,optionBTxtPhy,optionCTxtPhy,optionDTxtPhy;
        ImageView questionPhymgI,optionAPhymgI,optionBPhymgI,optionCImgI,optionDImgI;

        public MyHolder(View itemView) {
            super(itemView);

            questionTxtPhy=(TextView)itemView.findViewById(R.id.question_txtView);
            optionATxtPhy=(Button) itemView.findViewById(R.id.optionA);
            optionBTxtPhy=(Button)itemView.findViewById(R.id.optionB);
            optionCTxtPhy=(Button) itemView.findViewById(R.id.optionC);
            optionDTxtPhy=(Button) itemView.findViewById(R.id.optionD);
            serialNoPhy=(TextView)itemView.findViewById(R.id.serialNo);
            questionPhymgI=(ImageView)itemView.findViewById(R.id.question_imgView);
            optionAPhymgI=(ImageView)itemView.findViewById(R.id.optionAImg);
            optionBPhymgI=(ImageView)itemView.findViewById(R.id.optionBImg);
            optionCImgI=(ImageView)itemView.findViewById(R.id.optionCImg);
            optionDImgI=(ImageView)itemView.findViewById(R.id.optionDImg);







        }
    }
}
