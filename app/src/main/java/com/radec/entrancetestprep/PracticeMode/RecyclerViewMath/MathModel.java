package com.radec.entrancetestprep.PracticeMode.RecyclerViewMath;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 2/17/17.
 */

public class MathModel implements Serializable {
    public String question, optionA,optionB,optionC,optionD,correctOption,no,type;
    public ImageView questionImg,optionAImg,optionBImg,optionCImg,optionDImg;

    public MathModel()
    {

    }
    MathModel(String question, String type, String optionA,String optionB,String optionC,String optionD,String correctOption,String no, ImageView questionImg,ImageView optionAImg,ImageView optionBImg,ImageView optionCImg,ImageView optionDImg)
    {
        this.question=question;
        this.optionA=optionA;
        this.optionB=optionB;
        this.optionC=optionC;
        this.optionD=optionD;
        this.no=no;
        this.type=type;
        this.correctOption=correctOption;
        this.questionImg=questionImg;
        this.optionAImg=optionAImg;
        this.optionBImg=optionBImg;
        this.optionCImg=optionCImg;
        this.optionDImg=optionDImg;
    }

    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
