package com.radec.entrancetestprep.PracticeMode.RecyclerViewChem;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.radec.entrancetestprep.PracticeMode.RecyclerViewMath.Adapter_Math;
import com.radec.entrancetestprep.PracticeMode.RecyclerViewMath.MathModel;
import com.radec.entrancetestprep.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tayyabataimur on 2/20/17.
 */

public class AdapterChem  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static HashMap<String,Integer> scoreChem=new HashMap<String, Integer>();

    private Context context;
    private LayoutInflater inflater;
    List<DataModelChem> chem_data= Collections.emptyList();
    DataModelChem chemModel;
    String correct;




    public AdapterChem(Context context,List<DataModelChem> chem_data)
    {
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.chem_data=chem_data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.card,parent,false);
        AdapterChem.MyHolder holder=new AdapterChem.MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final AdapterChem.MyHolder myHolder=(AdapterChem.MyHolder)holder;

        chemModel=chem_data.get(position);
        myHolder.serialNoC.setText(String.valueOf(position + 1) + ". ");

        if(chem_data.get(position).typeC.equals(String.valueOf(0))) {

            myHolder.questionImgC.setVisibility(View.INVISIBLE);
            myHolder.optionAImgC.setVisibility(View.INVISIBLE);
            myHolder.optionBImgC.setVisibility(View.INVISIBLE);
            myHolder.optionCImgC.setVisibility(View.INVISIBLE);
            myHolder.optionDImgC.setVisibility(View.INVISIBLE);
            ColorDrawable optionATxtColor=(ColorDrawable) myHolder.optionATxtC.getBackground();
            ColorDrawable optionBTxtColor=(ColorDrawable) myHolder.optionATxtC.getBackground();
            ColorDrawable optionCTxtColor=(ColorDrawable) myHolder.optionATxtC.getBackground();
            ColorDrawable optionDTxtColor=(ColorDrawable) myHolder.optionATxtC.getBackground();


            if(optionATxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionBTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionCTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionDTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }

            myHolder.questionTxtC.setText(chemModel.questionC);
            myHolder.optionATxtC.setText(chemModel.optionAC);
            myHolder.optionBTxtC.setText(chemModel.optionBC);
            myHolder.optionCTxtC.setText(chemModel.optionCC);
            myHolder.optionDTxtC.setText(chemModel.optionDC);

            final int[] count = new int[1];

            myHolder.optionATxtC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    myHolder.optionBTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtC.setBackgroundColor(Color.parseColor("#4A148C"));

                    correct = "A";
                    if (correct.equals(chem_data.get(position).correctOptionC)) {
                        myHolder.optionATxtC.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreChem.put(String.valueOf(position), 1);


                    } else {
                        // Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionATxtC.setBackgroundColor(Color.parseColor("#f44336"));

                        scoreChem.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionBTxtC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionATxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "B";
                    if (correct.equals(chem_data.get(position).correctOptionC)) {
                        myHolder.optionBTxtC.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreChem.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionBTxtC.setBackgroundColor(Color.parseColor("#f44336"));

                        scoreChem.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionCTxtC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "C";
                    if (correct.equals(chem_data.get(position).correctOptionC)) {
                        myHolder.optionCTxtC.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreChem.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");


                    } else {
                        //  Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionCTxtC.setBackgroundColor(Color.parseColor("#f44336"));

                        scoreChem.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionDTxtC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxtC.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "D";
                    if (correct.equals(chem_data.get(position).correctOptionC)) {
                        myHolder.optionDTxtC.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreChem.put(String.valueOf(position), 1);

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        scoreChem.put(String.valueOf(position), 0);
                        myHolder.optionDTxtC.setBackgroundColor(Color.parseColor("#f44336"));

                    }

                }
            });
        }

        else
        {
            myHolder.questionImgC.setVisibility(View.VISIBLE);
            myHolder.optionAImgC.setVisibility(View.VISIBLE);
            myHolder.optionBImgC.setVisibility(View.VISIBLE);
            myHolder.optionCImgC.setVisibility(View.VISIBLE);
            myHolder.optionDImgC.setVisibility(View.VISIBLE);

            myHolder.questionTxtC.setVisibility(View.INVISIBLE);
            myHolder.optionATxtC.setVisibility(View.INVISIBLE);
            myHolder.optionBTxtC.setVisibility(View.INVISIBLE);
            myHolder.optionCTxtC.setVisibility(View.INVISIBLE);
            myHolder.optionDTxtC.setVisibility(View.INVISIBLE);


            String filename1 = chem_data.get(position).questionC;
            String baseDir1 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir1 + File.separator + filename1)).into(myHolder.questionImgC);

            String filename2 = chem_data.get(position).optionAC;
            String baseDir2 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir2 + File.separator + filename2)).into(myHolder.optionAImgC);

            String filename3 = chem_data.get(position).optionBC;
            String baseDir3 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir3 + File.separator + filename3)).into(myHolder.optionBImgC);
            //Drawable drawable= ContextCompat.getDrawable(context,Integer.parseInt(s));
            //myHolder.optionAImg.setImageDrawable(drawable);

            String filename4 = chem_data.get(position).optionAC;
            String baseDir4 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir4 + File.separator + filename4)).into(myHolder.optionCImgC);

            String filename5 = chem_data.get(position).optionAC;
            String baseDir5 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir5 + File.separator + filename5)).into(myHolder.optionDImgC);
            //Toast.makeText(context,s,Toast.LENGTH_SHORT).show();
            Drawable drawableBack=Drawable.createFromPath(String.valueOf(R.drawable.border));
            final Drawable drawableCorrect=Drawable.createFromPath(String.valueOf(R.drawable.bored_correct));
            final Drawable drawableInc=Drawable.createFromPath(String.valueOf(R.drawable.border_incorrect));

            myHolder.questionImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionAImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionBImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionCImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionDImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));

            myHolder.optionAImgC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="A";
                    myHolder.optionBImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(chem_data.get(position).correctOptionC.equals(correct))
                    {
                        scoreChem.put(String.valueOf(position), 1);

                        myHolder.optionAImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreChem.put(String.valueOf(position), 0);

                        myHolder.optionAImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionBImgC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="B";
                    myHolder.optionAImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(chem_data.get(position).correctOptionC.equals(correct))
                    {
                        scoreChem.put(String.valueOf(position), 1);

                        myHolder.optionBImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreChem.put(String.valueOf(position), 0);

                        myHolder.optionBImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionCImgC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="C";
                    myHolder.optionAImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(chem_data.get(position).correctOptionC.equals(correct))
                    {
                        scoreChem.put(String.valueOf(position), 1);

                        myHolder.optionCImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreChem.put(String.valueOf(position), 0);

                        myHolder.optionCImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionDImgC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="D";
                    myHolder.optionAImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(chem_data.get(position).correctOptionC.equals(correct))
                    {
                        scoreChem.put(String.valueOf(position), 1);

                        myHolder.optionDImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreChem.put(String.valueOf(position), 0);

                        myHolder.optionDImgC.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });


        }


    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return chem_data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{


        TextView questionTxtC,serialNoC;
        Button optionATxtC,optionBTxtC,optionCTxtC,optionDTxtC;
        ImageView questionImgC,optionAImgC,optionBImgC,optionCImgC,optionDImgC;

        public MyHolder(View itemView) {
            super(itemView);

            questionTxtC=(TextView)itemView.findViewById(R.id.question_txtView);
            optionATxtC=(Button) itemView.findViewById(R.id.optionA);
            optionBTxtC=(Button)itemView.findViewById(R.id.optionB);
            optionCTxtC=(Button) itemView.findViewById(R.id.optionC);
            optionDTxtC=(Button) itemView.findViewById(R.id.optionD);
            serialNoC=(TextView)itemView.findViewById(R.id.serialNo);
            questionImgC=(ImageView)itemView.findViewById(R.id.question_imgView);
            optionAImgC=(ImageView)itemView.findViewById(R.id.optionAImg);
            optionBImgC=(ImageView)itemView.findViewById(R.id.optionBImg);
            optionCImgC=(ImageView)itemView.findViewById(R.id.optionCImg);
            optionDImgC=(ImageView)itemView.findViewById(R.id.optionDImg);







        }
    }
}
