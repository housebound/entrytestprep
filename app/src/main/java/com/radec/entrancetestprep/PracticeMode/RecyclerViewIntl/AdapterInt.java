package com.radec.entrancetestprep.PracticeMode.RecyclerViewIntl;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.radec.entrancetestprep.PracticeMode.RecyclerViewEng.AdapterEng;
import com.radec.entrancetestprep.PracticeMode.RecyclerViewEng.DataModelEng;
import com.radec.entrancetestprep.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tayyabataimur on 2/20/17.
 */

public class AdapterInt extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static HashMap<String,Integer> scoreInt=new HashMap<String, Integer>();

    private Context context;
    private LayoutInflater inflater;
    List<DataModelInt> int_data= Collections.emptyList();
    DataModelInt intModel;
    String correct;




    public AdapterInt(Context context,List<DataModelInt> int_data)
    {
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.int_data=int_data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.card,parent,false);
        AdapterInt.MyHolder holder=new AdapterInt.MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final AdapterInt.MyHolder myHolder=(AdapterInt.MyHolder)holder;

        intModel=int_data.get(position);
        myHolder.serialNoI.setText(String.valueOf(position + 1) + ". ");

        if(int_data.get(position).typeI.equals(String.valueOf(0))) {

            myHolder.questionImgI.setVisibility(View.INVISIBLE);
            myHolder.optionAImgI.setVisibility(View.INVISIBLE);
            myHolder.optionBImgI.setVisibility(View.INVISIBLE);
            myHolder.optionCImgI.setVisibility(View.INVISIBLE);
            myHolder.optionDImgI.setVisibility(View.INVISIBLE);
            ColorDrawable optionATxtColor=(ColorDrawable) myHolder.optionATxtI.getBackground();
            ColorDrawable optionBTxtColor=(ColorDrawable) myHolder.optionATxtI.getBackground();
            ColorDrawable optionCTxtColor=(ColorDrawable) myHolder.optionATxtI.getBackground();
            ColorDrawable optionDTxtColor=(ColorDrawable) myHolder.optionATxtI.getBackground();


            if(optionATxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionBTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionCTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionDTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }

            myHolder.questionTxtI.setText(intModel.questionI);
            myHolder.optionATxtI.setText(intModel.optionAI);
            myHolder.optionBTxtI.setText(intModel.optionBI);
            myHolder.optionCTxtI.setText(intModel.optionCI);
            myHolder.optionDTxtI.setText(intModel.optionDI);

            final int[] count = new int[1];

            myHolder.optionATxtI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    myHolder.optionBTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtI.setBackgroundColor(Color.parseColor("#4A148C"));

                    correct = "A";
                    if (correct.equals(int_data.get(position).correctOptionI)) {
                        myHolder.optionATxtI.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreInt.put(String.valueOf(position), 1);


                    } else {
                        // Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionATxtI.setBackgroundColor(Color.parseColor("#f44336"));

                        scoreInt.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionBTxtI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionATxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "B";
                    if (correct.equals(int_data.get(position).correctOptionI)) {
                        myHolder.optionBTxtI.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreInt.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionBTxtI.setBackgroundColor(Color.parseColor("#f44336"));

                        scoreInt.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionCTxtI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "C";
                    if (correct.equals(int_data.get(position).correctOptionI)) {
                        myHolder.optionCTxtI.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreInt.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");


                    } else {
                        //  Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionCTxtI.setBackgroundColor(Color.parseColor("#f44336"));

                        scoreInt.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionDTxtI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxtI.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "D";
                    if (correct.equals(int_data.get(position).correctOptionI)) {
                        myHolder.optionDTxtI.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        scoreInt.put(String.valueOf(position), 1);

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        scoreInt.put(String.valueOf(position), 0);
                        myHolder.optionDTxtI.setBackgroundColor(Color.parseColor("#f44336"));

                    }

                }
            });
        }

        else
        {
            myHolder.questionImgI.setVisibility(View.VISIBLE);
            myHolder.optionAImgI.setVisibility(View.VISIBLE);
            myHolder.optionBImgI.setVisibility(View.VISIBLE);
            myHolder.optionCImgI.setVisibility(View.VISIBLE);
            myHolder.optionDImgI.setVisibility(View.VISIBLE);

            myHolder.questionTxtI.setVisibility(View.INVISIBLE);
            myHolder.optionATxtI.setVisibility(View.INVISIBLE);
            myHolder.optionBTxtI.setVisibility(View.INVISIBLE);
            myHolder.optionCTxtI.setVisibility(View.INVISIBLE);
            myHolder.optionDTxtI.setVisibility(View.INVISIBLE);


            String filename1 = int_data.get(position).questionI;
            String baseDir1 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir1 + File.separator + filename1)).into(myHolder.questionImgI);

            String filename2 = int_data.get(position).optionAI;
            String baseDir2 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir2 + File.separator + filename2)).into(myHolder.optionAImgI);

            String filename3 = int_data.get(position).optionBI;
            String baseDir3 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir3 + File.separator + filename3)).into(myHolder.optionBImgI);
            //Drawable drawable= ContextCompat.getDrawable(context,Integer.parseInt(s));
            //myHolder.optionAImg.setImageDrawable(drawable);

            String filename4 = int_data.get(position).optionAI;
            String baseDir4 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir4 + File.separator + filename4)).into(myHolder.optionCImgI);

            String filename5 = int_data.get(position).optionAI;
            String baseDir5 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir5 + File.separator + filename5)).into(myHolder.optionDImgI);
            //Toast.makeText(context,s,Toast.LENGTH_SHORT).show();
            Drawable drawableBack=Drawable.createFromPath(String.valueOf(R.drawable.border));
            final Drawable drawableCorrect=Drawable.createFromPath(String.valueOf(R.drawable.bored_correct));
            final Drawable drawableInc=Drawable.createFromPath(String.valueOf(R.drawable.border_incorrect));

            myHolder.questionImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionAImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionBImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));

            myHolder.optionAImgI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="A";
                    myHolder.optionBImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(int_data.get(position).correctOptionI.equals(correct))
                    {
                        scoreInt.put(String.valueOf(position), 1);

                        myHolder.optionAImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreInt.put(String.valueOf(position), 0);

                        myHolder.optionAImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionBImgI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="B";
                    myHolder.optionAImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(int_data.get(position).correctOptionI.equals(correct))
                    {
                        scoreInt.put(String.valueOf(position), 1);

                        myHolder.optionBImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreInt.put(String.valueOf(position), 0);

                        myHolder.optionBImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionCImgI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="C";
                    myHolder.optionAImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(int_data.get(position).correctOptionI.equals(correct))
                    {
                        scoreInt.put(String.valueOf(position), 1);

                        myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreInt.put(String.valueOf(position), 0);

                        myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionDImgI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="D";
                    myHolder.optionAImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(int_data.get(position).correctOptionI.equals(correct))
                    {
                        scoreInt.put(String.valueOf(position), 1);

                        myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        scoreInt.put(String.valueOf(position), 0);

                        myHolder.optionDImgI.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });


        }


    }
    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return int_data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{


        TextView questionTxtI,serialNoI;
        Button optionATxtI,optionBTxtI,optionCTxtI,optionDTxtI;
        ImageView questionImgI,optionAImgI,optionBImgI,optionCImgI,optionDImgI;

        public MyHolder(View itemView) {
            super(itemView);

            questionTxtI=(TextView)itemView.findViewById(R.id.question_txtView);
            optionATxtI=(Button) itemView.findViewById(R.id.optionA);
            optionBTxtI=(Button)itemView.findViewById(R.id.optionB);
            optionCTxtI=(Button) itemView.findViewById(R.id.optionC);
            optionDTxtI=(Button) itemView.findViewById(R.id.optionD);
            serialNoI=(TextView)itemView.findViewById(R.id.serialNo);
            questionImgI=(ImageView)itemView.findViewById(R.id.question_imgView);
            optionAImgI=(ImageView)itemView.findViewById(R.id.optionAImg);
            optionBImgI=(ImageView)itemView.findViewById(R.id.optionBImg);
            optionCImgI=(ImageView)itemView.findViewById(R.id.optionCImg);
            optionDImgI=(ImageView)itemView.findViewById(R.id.optionDImg);







        }
    }
}
