package com.radec.entrancetestprep.PracticeMode.RecyclerViewChem;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 2/20/17.
 */

public class DataModelChem implements Serializable {
    public String questionC, optionAC,optionBC,optionCC,optionDC,correctOptionC,noC,typeC;
    public ImageView questionImgC,optionAImgC,optionBImgC,optionCImgC,optionDImgC;

    public DataModelChem()
    {

    }
    DataModelChem(String questionC,String optionAC,String optionBC,String optionCC,String optionDC,String correctOptionC,String noC,ImageView questionImgC,ImageView optionAImgC,ImageView optionBImgC,ImageView optionCImgC,ImageView optionDImgC, String typeC)
    {
        this.questionC=questionC;
        this.optionAC=optionAC;
        this.optionBC=optionBC;
        this.optionCC=optionCC;
        this.optionDC=optionDC;
        this.noC=noC;
        this.correctOptionC=correctOptionC;
        this.typeC=typeC;
        this.correctOptionC=correctOptionC;
        this.questionImgC=questionImgC;
        this.optionAImgC=optionAImgC;
        this.optionBImgC=optionBImgC;
        this.optionCImgC=optionCImgC;
        this.optionDImgC=optionDImgC;
    }

    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
