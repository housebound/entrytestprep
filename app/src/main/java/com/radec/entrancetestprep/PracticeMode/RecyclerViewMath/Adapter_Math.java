package com.radec.entrancetestprep.PracticeMode.RecyclerViewMath;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.radec.entrancetestprep.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tayyabataimur on 2/17/17.
 */

public class Adapter_Math extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static HashMap<String,Integer> score=new HashMap<String, Integer>();

    private Context context;
    private LayoutInflater inflater;
    List<MathModel> math_data= Collections.emptyList();
    MathModel mathModel;
    String correct;




    public Adapter_Math(Context context,List<MathModel> math_data)
    {
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.math_data=math_data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.card,parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final MyHolder myHolder=(MyHolder)holder;

        mathModel=math_data.get(position);
        myHolder.serialNo.setText(String.valueOf(position + 1) + ". ");

        if(math_data.get(position).type.equals(String.valueOf(0))) {

            myHolder.questionImg.setVisibility(View.INVISIBLE);
            myHolder.optionAImg.setVisibility(View.INVISIBLE);
            myHolder.optionBImg.setVisibility(View.INVISIBLE);
            myHolder.optionCImg.setVisibility(View.INVISIBLE);
            myHolder.optionDImg.setVisibility(View.INVISIBLE);
            ColorDrawable optionATxtColor=(ColorDrawable) myHolder.optionATxt.getBackground();
            ColorDrawable optionBTxtColor=(ColorDrawable) myHolder.optionATxt.getBackground();
            ColorDrawable optionCTxtColor=(ColorDrawable) myHolder.optionATxt.getBackground();
            ColorDrawable optionDTxtColor=(ColorDrawable) myHolder.optionATxt.getBackground();


            if(optionATxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionBTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionCTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }
            if(optionDTxtColor.toString().equals(String.valueOf((Color.parseColor("#4A148C")))))
            {
                myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
            }
            else {

                //do nothing

            }

            myHolder.questionTxt.setText(mathModel.question);
            myHolder.optionATxt.setText(mathModel.optionA);
            myHolder.optionBTxt.setText(mathModel.optionB);
            myHolder.optionCTxt.setText(mathModel.optionC);
            myHolder.optionDTxt.setText(mathModel.optionD);

            final int[] count = new int[1];

            myHolder.optionATxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));

                    correct = "A";
                    if (correct.equals(math_data.get(position).correctOption)) {
                        myHolder.optionATxt.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        score.put(String.valueOf(position), 1);


                    } else {
                        // Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionATxt.setBackgroundColor(Color.parseColor("#f44336"));

                        score.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionBTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "B";
                    if (correct.equals(math_data.get(position).correctOption)) {
                        myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        score.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#f44336"));

                        score.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionCTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "C";
                    if (correct.equals(math_data.get(position).correctOption)) {
                        myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        score.put(String.valueOf(position), 1);
                        // count[0][0] = Collections.frequency(new ArrayList<String>(score.values()), "1");


                    } else {
                        //  Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#f44336"));

                        score.put(String.valueOf(position), 0);
                    }

                }
            });

            myHolder.optionDTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myHolder.optionBTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionCTxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    myHolder.optionATxt.setBackgroundColor(Color.parseColor("#4A148C"));
                    correct = "D";
                    if (correct.equals(math_data.get(position).correctOption)) {
                        myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#8BC34A"));

                        // Toast.makeText(context,"correct",Toast.LENGTH_SHORT).show();
                        score.put(String.valueOf(position), 1);

                    } else {
                        //Toast.makeText(context,"incorrect",Toast.LENGTH_SHORT).show();
                        score.put(String.valueOf(position), 0);
                        myHolder.optionDTxt.setBackgroundColor(Color.parseColor("#f44336"));

                    }

                }
            });
        }

        else
        {
            myHolder.questionImg.setVisibility(View.VISIBLE);
            myHolder.optionAImg.setVisibility(View.VISIBLE);
            myHolder.optionBImg.setVisibility(View.VISIBLE);
            myHolder.optionCImg.setVisibility(View.VISIBLE);
            myHolder.optionDImg.setVisibility(View.VISIBLE);

            myHolder.questionTxt.setVisibility(View.INVISIBLE);
            myHolder.optionATxt.setVisibility(View.INVISIBLE);
            myHolder.optionBTxt.setVisibility(View.INVISIBLE);
            myHolder.optionCTxt.setVisibility(View.INVISIBLE);
            myHolder.optionDTxt.setVisibility(View.INVISIBLE);


            String filename1 = math_data.get(position).question;
            String baseDir1 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir1 + File.separator + filename1)).into(myHolder.questionImg);

            String filename2 = math_data.get(position).optionA;
            String baseDir2 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir2 + File.separator + filename2)).into(myHolder.optionAImg);

            String filename3 = math_data.get(position).optionB;
            String baseDir3 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir3 + File.separator + filename3)).into(myHolder.optionBImg);
            //Drawable drawable= ContextCompat.getDrawable(context,Integer.parseInt(s));
            //myHolder.optionAImg.setImageDrawable(drawable);

            String filename4 = math_data.get(position).optionA;
            String baseDir4 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir4 + File.separator + filename4)).into(myHolder.optionCImg);

            String filename5 = math_data.get(position).optionA;
            String baseDir5 = Environment.getExternalStorageDirectory().getAbsolutePath();
            Picasso.with(context).load(new File(baseDir5 + File.separator + filename5)).into(myHolder.optionDImg);
            //Toast.makeText(context,s,Toast.LENGTH_SHORT).show();
            Drawable drawableBack=Drawable.createFromPath(String.valueOf(R.drawable.border));
            final Drawable drawableCorrect=Drawable.createFromPath(String.valueOf(R.drawable.bored_correct));
            final Drawable drawableInc=Drawable.createFromPath(String.valueOf(R.drawable.border_incorrect));

            myHolder.questionImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
            myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));

            myHolder.optionAImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="A";
                    myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(math_data.get(position).correctOption.equals(correct))
                    {
                        score.put(String.valueOf(position), 1);

                        myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        score.put(String.valueOf(position), 0);

                        myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionBImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="B";
                    myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(math_data.get(position).correctOption.equals(correct))
                    {
                        score.put(String.valueOf(position), 1);

                        myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        score.put(String.valueOf(position), 0);

                        myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionCImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="C";
                    myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(math_data.get(position).correctOption.equals(correct))
                    {
                        score.put(String.valueOf(position), 1);

                        myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        score.put(String.valueOf(position), 0);

                        myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });

            myHolder.optionDImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    correct="D";
                    myHolder.optionAImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionBImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    myHolder.optionCImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border));
                    if(math_data.get(position).correctOption.equals(correct))
                    {
                        score.put(String.valueOf(position), 1);

                        myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.bored_correct));

                    }
                    else
                    {
                        score.put(String.valueOf(position), 0);

                        myHolder.optionDImg.setBackground(ContextCompat.getDrawable(context,R.drawable.border_incorrect));

                    }
                }
            });


        }


    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return math_data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{


        TextView questionTxt,serialNo;
        Button optionATxt,optionBTxt,optionCTxt,optionDTxt;
        ImageView questionImg,optionAImg,optionBImg,optionCImg,optionDImg;

        public MyHolder(View itemView) {
            super(itemView);

            questionTxt=(TextView)itemView.findViewById(R.id.question_txtView);
            optionATxt=(Button) itemView.findViewById(R.id.optionA);
            optionBTxt=(Button)itemView.findViewById(R.id.optionB);
            optionCTxt=(Button) itemView.findViewById(R.id.optionC);
            optionDTxt=(Button) itemView.findViewById(R.id.optionD);
            serialNo=(TextView)itemView.findViewById(R.id.serialNo);
            questionImg=(ImageView)itemView.findViewById(R.id.question_imgView);
            optionAImg=(ImageView)itemView.findViewById(R.id.optionAImg);
            optionBImg=(ImageView)itemView.findViewById(R.id.optionBImg);
            optionCImg=(ImageView)itemView.findViewById(R.id.optionCImg);
            optionDImg=(ImageView)itemView.findViewById(R.id.optionDImg);







        }
    }
}
