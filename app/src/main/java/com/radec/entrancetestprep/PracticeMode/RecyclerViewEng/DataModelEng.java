package com.radec.entrancetestprep.PracticeMode.RecyclerViewEng;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by tayyabataimur on 2/20/17.
 */

public class DataModelEng implements Serializable {
    public String questionE, optionAE,optionBE,optionCE,optionDE,correctOptionE,noE,typeE;
    public ImageView questionImgE,optionAImgE,optionBImgE,optionCImgE,optionDImgE;

    public DataModelEng()
    {

    }
    DataModelEng(String questionE,String optionAE,String optionBE,String optionCE,String optionDE,String correctOptionE,String noE,ImageView questionImgE,ImageView optionAImgE,ImageView optionBImgE,ImageView optionCImgE,ImageView optionDImgE, String typeE)
    {
        this.questionE=questionE;
        this.optionAE=optionAE;
        this.optionBE=optionBE;
        this.optionCE=optionCE;
        this.optionDE=optionDE;
        this.noE=noE;
        this.correctOptionE=correctOptionE;
        this.typeE=typeE;
        this.questionImgE=questionImgE;
        this.optionAImgE=optionAImgE;
        this.optionBImgE=optionBImgE;
        this.optionCImgE=optionCImgE;
        this.optionDImgE=optionDImgE;

    }

    private int checkID=-1;

    public void setSelectedRadioButton(int checkID)
    {
        this.checkID=checkID;
    }

    public int getSelectedRadioButton()
    {
        return checkID;
    }


}
